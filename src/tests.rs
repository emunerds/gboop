#[cfg(test)]
mod tests {
    use crate::helper::*;

    #[test]
    fn test_split_u16() {
        assert_eq!(
            split_u16(0xDEAD),
            (0xDE, 0xAD)
        );
    }

    #[test]
    fn test_bind_u16() {
        assert_eq!(
            bind_u16(0x13, 0x37),
            0x1337
        );
    }

    #[test]
    fn test_addr_to_nibbles() {
        assert_eq!(
            get_address_nibbles(0xACDE),
            (0xA, 0xC, 0xD, 0xE)
        );
    }

    #[test]
    fn test_nibbles_to_addr() {
        assert_eq!(
            nibbles_to_addr(0x1, 0x2, 0x3, 0x4),
            0x1234
        );
    }
}
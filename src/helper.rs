use std::mem::transmute;

pub fn split_u16(value: u16) -> (u8, u8) {
    (
        ((value & 0xFF00) >> 8) as u8,
        (value & 0x00FF) as u8
    )
}

pub fn bind_u16(v1: u8, v2: u8) -> u16 {
    ((v1 as u16) << 8) | v2 as u16
}

pub fn get_address_nibbles(addr: u16) -> (u8, u8, u8, u8) {
    let bytes: [u8; 2] = unsafe { transmute(addr.to_be()) };

    (
        (bytes[0] & 0xF0) >> 4,
        (bytes[0] & 0x0F),
        (bytes[1] & 0xF0) >> 4,
        (bytes[1] & 0x0F)
    )
}

pub fn nibbles_to_addr(n0: u8, n1: u8, n2: u8, n3: u8) -> u16 {
    let addr: u16
        = (n0 as u16) << 12
        | (n1 as u16) << 8
        | (n2 as u16) << 4
        | (n3 as u16);

    addr
}

use crate::memory;
use crate::registers;

#[allow(dead_code)]
pub struct CPU {
    pub registers: registers::Registers,
    pub program_counter: u16,
    pub memory_bus: memory::MemoryBus,
}

impl CPU {
    pub fn new() -> Self {
        CPU {
            registers: registers::Registers::new(),
            program_counter: 0x0000,
            memory_bus: memory::MemoryBus::new(),
        }
    }

    pub fn step(&mut self) {
        // fetch
        let _instruction = self.memory_bus.get(self.program_counter);

        // decode

        // execute
    }
}


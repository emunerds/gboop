use crate::helper::*;

#[allow(dead_code)]
pub struct Registers {
    a: u8,
    b: u8,
    c: u8,
    d: u8,
    e: u8,
    f: Flags,
    h: u8,
    l: u8,
}

#[allow(dead_code)]
impl Registers {
    // initialization
    pub fn new() -> Self {
        Registers {
            a: 0,
            b: 0,
            c: 0,
            d: 0,
            e: 0,
            f: Flags::from(0),
            h: 0,
            l: 0,
        }
    }
    // 16-bit register accessors
    // AF
    pub fn get_af(&self) -> u16 {
        bind_u16(self.a, u8::from(self.f.clone()))
    }

    pub fn set_af(&mut self, value: u16) {
        let (a, f) = split_u16(value);
        self.a = a;
        self.f = Flags::from(f);
    }

    // BC
    pub fn get_bc(&self) -> u16 {
        bind_u16(self.b, self.c)
    }

    pub fn set_bc(&mut self, value: u16) {
        let (b, c) = split_u16(value);
        self.b = b;
        self.c = c;
    }

    // DE
    pub fn get_de(&self) -> u16 {
        bind_u16(self.d, self.e)
    }

    pub fn set_de(&mut self, value: u16) {
        let (d, e) = split_u16(value);
        self.d = d;
        self.e = e;
    }

    // HL
    pub fn get_hl(&self) -> u16 {
        bind_u16(self.h, self.l)
    }

    pub fn set_hl(&mut self, value: u16) {
        let (h, l) = split_u16(value);
        self.h = h;
        self.l = l;
    }
}

/// `Flags` represents the CPU's flags register. There are four flags, mapped to the four upper
/// bits of the register. the lower four bits always have a value of 0.
#[derive(Copy, Clone)]
struct Flags {
    zero: bool,
    subtract: bool,
    half_carry: bool,
    carry: bool,
}

const FLAGS_ZERO_POS: u8 = 7;
const FLAGS_SUBTRACT_POS: u8 = 6;
const FLAGS_HALF_CARRY_POS: u8 = 5;
const FLAGS_CARRY_POS: u8 = 4;

impl std::convert::From<u8> for Flags {
    fn from(byte: u8) -> Self {
        let zero        = (byte >> FLAGS_ZERO_POS & 1) != 0;
        let subtract    = (byte >> FLAGS_SUBTRACT_POS & 1) != 0;
        let half_carry  = (byte >> FLAGS_HALF_CARRY_POS & 1) != 0;
        let carry       = (byte >> FLAGS_CARRY_POS & 1) != 0;

        Flags { zero, subtract, half_carry, carry }
    }
}

impl std::convert::From<Flags> for u8 {
    fn from(flags: Flags) -> Self {
        (flags.zero as u8) << FLAGS_ZERO_POS |
            (flags.subtract as u8) << FLAGS_SUBTRACT_POS |
            (flags.half_carry as u8) << FLAGS_HALF_CARRY_POS |
            (flags.carry as u8) << FLAGS_CARRY_POS
    }
}


use crate::helper::*;

const BANK_SIZE: usize = 0x4000;
const TILE_RAM_SIZE: usize = 0x1800;
const BACKGROUND_RAM_SIZE: usize = 0x800;
const CARTRIDGE_RAM_SIZE: usize = 0x2000;
const WORK_RAM_SIZE: usize = 0x2000;
const IO_SIZE: usize = 0x80;
const OAM_SIZE: usize = 0xA0;
const HIGH_RAM_SIZE: usize = 0x80;

/// `MemoryBus` Represents the memory address space available to the CPU.
/// This memory is mapped as follows:
/// 0x0000 -> 0x00FF: Boot ROM
/// 0x0000 -> 0x3FFF: ROM Bank 0
/// 0x4000 -> 0x7FFF: ROM Bank N
/// 0x8000 -> 0x97FF: Tile RAM
/// 0x9800 -> 0x9FFF: Background Map
/// 0xA000 -> 0xBFFF: Cartridge RAM
/// 0xC000 -> 0xDFFF: Work RAM
/// 0xE000 -> 0xFDFF: Echo RAM (behaves identically to accessing work ram)
/// 0xFE00 -> 0xFE9F: OAM (Object Attribute Memory)
/// 0xFEA0 -> 0xFEFF: Unused
/// 0xFF00 -> 0xFF7F: IO Registers
/// 0xFF80 -> 0xFFFE: High RAM
/// 0xFFFF -> Interrupt Register
///
/// Non-trivial memory spaces should be replaced with structs at some point.
#[allow(dead_code)]
pub struct MemoryBus {
    bank_zero:      [u8; BANK_SIZE],
    bank_n:         [u8; BANK_SIZE],
    tiles:          [u8; TILE_RAM_SIZE],
    background:     [u8; BACKGROUND_RAM_SIZE],
    cartridge_ram:  [u8; CARTRIDGE_RAM_SIZE],
    work_ram:       [u8; WORK_RAM_SIZE],
    io:             [u8; IO_SIZE],
    oam:            [u8; OAM_SIZE],
    high_ram:       [u8; HIGH_RAM_SIZE],
}

impl MemoryBus {
    pub fn new() -> Self {
        MemoryBus {
            bank_zero:      [0; BANK_SIZE],
            bank_n:         [0; BANK_SIZE],
            tiles:          [0; TILE_RAM_SIZE],
            background:     [0; BACKGROUND_RAM_SIZE],
            cartridge_ram:  [0; CARTRIDGE_RAM_SIZE],
            work_ram:       [0; WORK_RAM_SIZE],
            io:             [0; IO_SIZE],
            oam:            [0; OAM_SIZE],
            high_ram:       [0; HIGH_RAM_SIZE],
        }
    }
    pub fn get(&self, addr: u16) -> u8 {
        match get_address_nibbles(addr) {
            // 0x0000 -> 0x3FFF
            (0x0, _, _, _) => 0, // TODO: Bank 0
            (0x1, _, _, _) => 0, // TODO: Bank 0
            (0x2, _, _, _) => 0, // TODO: Bank 0
            (0x3, _, _, _) => 0, // TODO: Bank 0

            // 0x4000 -> 0x7FFF
            (0x4, _, _, _) => 0, // TODO: Bank N
            (0x5, _, _, _) => 0, // TODO: Bank N
            (0x6, _, _, _) => 0, // TODO: Bank N
            (0x7, _, _, _) => 0, // TODO: Bank N

            // 0x8000 -> 0x9FFF
            (0x8, _, _, _) => 0, // TODO: Tile RAM
            (0x9, n1, _, _) => {
                if n1 < 0x8 {
                    0 // TODO: Tile RAM
                }
                else {
                    0 // TODO: Background Map
                }
            },

            // 0xA000 -> 0xBFFF
            (0xA, _, _, _) => 0, // TODO: Cartridge RAM
            (0xB, _, _, _) => 0, // TODO: Cartridge RAM

            // 0xC000 -> 0xDFFF
            (0xC, _, _, _) => self.read_wram(0x1FFF & addr),
            (0xD, _, _, _) => self.read_wram(0x1FFF & addr),

            // 0xE000 -> 0xFFFF
            (0xE, _, _, _) => self.read_wram(0x1FFF & addr),
            (0xF, 0xE, n2, _) => {
                if n2 < 0xA {
                    0 // TODO: OAM
                }
                else {
                    0 // unused space
                }
            },
            (0xF, 0xF, n2, _) => {
                if n2 < 0x8 {
                    0 // TODO: IO Registers
                }
                else {
                    0 // TODO: High RAM
                }
            },
            (0xF, _, _, _) => self.read_wram(0x1FFF & addr),

            // fallback behaviour is always to return 0.
            _ => 0
        }
    }

    pub fn write(&mut self, addr: u16, value: u8) {
        match get_address_nibbles(addr) {
            // 0x0000 -> 0x7FFF are ROM and can be safely ignored.

            // 0xC000 -> 0xDFFF
            (0xC, _, _, _) => self.write_wram(addr & 0x1FFF, value),
            (0xD, _, _, _) => self.write_wram(addr & 0x1FFF, value),

            // 0xE000 -> 0xFFFF
            (0xE, _, _, _) => self.write_wram(addr & 0x1FFF, value),
            (0xF, _, _, _) => self.write_wram(addr & 0x1FFF, value),
            _ => ()
        }
    }

    fn write_wram(&mut self, addr: u16, value: u8) {
        self.work_ram[addr as usize] = value;
    }

    fn read_wram(&self, addr: u16) -> u8 {
        self.work_ram[addr as usize]
    }
}

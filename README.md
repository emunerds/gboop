# ![gboop](gboop.svg)

gb emulator written in rust!

## documentation

- http://marc.rawer.de/Gameboy/Docs/GBCPUman.pdf
- https://gekkio.fi/files/gb-docs/gbctr.pdf
- https://www.pastraiser.com/cpu/gameboy/gameboy_opcodes.html
- https://gbdev.io/pandocs/

## useful crates

- https://github.com/Gekkio/imgui-rs
- https://docs.rs/winit/0.22.1/winit/
- https://crates.io/crates/gfx

## useful links

- https://cturt.github.io/cinoop.html
